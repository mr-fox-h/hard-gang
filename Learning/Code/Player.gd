extends KinematicBody2D

### Initialize ###

export (int) var Ammo = 3

const BULLET_PATH_1 = preload("res://Bullet_1.tscn")
const BULLET_PATH_2 = preload("res://Bullet_2.tscn")
const UP = Vector2(0, -1)
const ACCELERATION = 150.0
const MAX_SPEED = 150.0
const GRAVITY = 20.0
const JUMP_HIGHT = -350.0

var Motion = Vector2()
var Blood_1 = load("res://Hit_1.tscn")
var Blood_2 = load("res://Hit_2.tscn")
var HP = 5

### Base Code ###

func _physics_process(delta):
	$Fire_1.play("None")
	$Fire_2.play("None")
	$Sprite/Ammo_Progress.value = Ammo
	var Friction = false
	Motion.y += GRAVITY
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
	if Input.is_action_pressed("Move_Right"):
		Motion.x = min(Motion.x + ACCELERATION, MAX_SPEED)
		$Sprite.flip_h = false
		$Sprite.play("Run")
	elif Input.is_action_pressed("Move_Left"):
		Motion.x = max(Motion.x - ACCELERATION, -MAX_SPEED)
		$Sprite.flip_h = true
		$Sprite.play("Run")
	else:
		Friction = true
		$Sprite.play("Idle")
	if is_on_floor():
		if Input.is_action_just_pressed("Jump"):
			Motion.y = JUMP_HIGHT
			$Sprite/Jump_SFX.play()
		if Friction == true:
			Motion.x = lerp(Motion.x, 0, 0.5)
	else:
		$Sprite.play("Jump")
	if Ammo == 0:
		$Sprite/Ammo_Progress.value = 0
		if Ammo == 0 and Input.is_action_just_pressed("Shoot"):
			$Sprite/Empty_Gun_SFX.play()
	else:
		if Input.is_action_just_pressed("Shoot") and $Sprite.flip_h == false and $Gun_Timer.time_left == 0:
			$Sprite.play("Shoot")
			$Sprite/Gun_SFX.play()
			Ammo -= 1
			$Sprite/Ammo_Progress.value = Ammo
			$Fire_1.play("Fire")
			_Fire_1()
		elif Input.is_action_just_pressed("Shoot") and $Sprite.flip_h == true and $Gun_Timer.time_left == 0:
			$Sprite.play("Shoot")
			$Sprite/Gun_SFX.play()
			Ammo -= 1
			$Sprite/Ammo_Progress.value = Ammo
			$Fire_2.play("Fire")
			_Fire_2()
	Motion = move_and_slide(Motion, UP)
	
### Die Aria ###

func _on_Die_body_entered(body):
	HP -= 1
	$Sprite/HP_Progress.value = HP
	if HP == 0:
		$CollisionShape2D.disabled = true
		queue_free()
		get_tree().change_scene("res://Test.tscn")
	else:
		var Blood_1_Instance = Blood_2.instance()
		var Blood_2_Instance = Blood_1.instance()
		if $Sprite.flip_h == false:
			get_tree().current_scene.add_child(Blood_1_Instance)
			Blood_1_Instance.global_position = global_position
			Motion.y = -200
			Motion.x = -200
		else:
			get_tree().current_scene.add_child(Blood_2_Instance)
			Blood_2_Instance.global_position = global_position
			Motion.y = -200
			Motion.x = 200

### Bullet_1 ###

func _Fire_1():
	$Gun_Timer.start()
	var Bullet_1 = BULLET_PATH_1.instance()
	get_parent().add_child(Bullet_1)
	Bullet_1.position = $Bullet_Start_1.global_position

### Bullet_2 ###

func _Fire_2():
	$Gun_Timer.start()
	var Bullet_2 = BULLET_PATH_2.instance()
	get_parent().add_child(Bullet_2)
	Bullet_2.position = $Bullet_Start_2.global_position
