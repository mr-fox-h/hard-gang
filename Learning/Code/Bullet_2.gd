extends KinematicBody2D

var Motion = Vector2(1, 0)
var Speed = -500

func _physics_process(delta):
	var Collusion_Info = move_and_collide(Motion.normalized() * Speed * delta)
func _on_End_Timer_timeout():
	queue_free()
func _on_End_body_entered(body):
	$CollisionShape2D.disabled = true
	queue_free()
