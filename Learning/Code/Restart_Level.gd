extends Area2D

export(String, FILE, "*.tscn") var World_Scene_Restart

func _physics_process(delta):
	var Bodies = get_overlapping_bodies()
	for Body in Bodies:
		if Body.name == "Player":
			get_tree().change_scene(World_Scene_Restart)
