extends KinematicBody2D

var Is_Move_Left = true
var Gravity = 20.0
var Motion = Vector2()
var Speed = 20.0

func _ready():
	$Sprite.play("Run")
func _on_Stomp_body_entered(body):
	$CollisionShape2D.disabled = true
	queue_free()
func _process(delta):
	_Move()
	_Turn_Around()
func _Move():
	Motion.x = +Speed if Is_Move_Left else -Speed
	Motion.y += Gravity
	Motion = move_and_slide(Motion, Vector2.UP)
func _Turn_Around():
	if not $RayCast2D.is_colliding() and is_on_floor():
		Is_Move_Left = !Is_Move_Left
		scale.x = -scale.x



