extends KinematicBody2D

var Is_Move_Right = false
var Gravity = 20.0
var Motion = Vector2()
var Speed = 20.0

func _ready():
	$Sprite.play("Run")
func _on_Explosive_body_entered(body):
	$CollisionShape2D.disabled = true
	queue_free()
func _process(delta):
	_Move()
func _Move():
	if Is_Move_Right:
		Motion.x = -Speed
	else:
		Motion.x = +Speed
	Motion.y += Gravity
	Motion = move_and_slide(Motion, Vector2.UP)
func _on_Turn_body_entered(body):
	Is_Move_Right = !Is_Move_Right
	scale.x = -scale.x
